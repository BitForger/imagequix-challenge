import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ENVALID } from 'nestjs-envalid';
import { Config } from './env/app.env';
import { Logger } from 'nestjs-pino';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(Logger));
  const env: Config = await app.resolve(ENVALID);

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(env.PORT);
}
bootstrap();
