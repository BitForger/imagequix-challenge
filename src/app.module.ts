import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LoggerModule, Params } from 'nestjs-pino';
import { Config, validators } from './env/app.env';
import { ENVALID, EnvalidModule } from 'nestjs-envalid';
import { MongooseConfigService } from './db/mongoose-config.service';
import { PostModule } from './post/post.module';
import { CommentModule } from './comment/comment.module';
import { Post, PostSchema } from './db/schema/post.schema';
import { Comment, CommentSchema } from './db/schema/comment.schema';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useClass: MongooseConfigService,
    }),
    MongooseModule.forFeature([
      {
        name: Post.name,
        schema: PostSchema,
      },
      {
        name: Comment.name,
        schema: CommentSchema,
      },
    ]),
    EnvalidModule.forRoot({ validators, useDotenv: true, isGlobal: true }),
    LoggerModule.forRootAsync({
      inject: [ENVALID],
      useFactory(env: Config): Params {
        return {
          pinoHttp: {
            useLogLevel: env.isDev ? 'debug' : 'info',
            prettyPrint: !!env.isDev,
          },
        };
      },
    }),
    PostModule,
    CommentModule,
  ],
  exports: [MongooseModule],
})
export class AppModule {}
