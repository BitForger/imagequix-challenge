import {
  MongooseModuleOptions,
  MongooseOptionsFactory,
} from '@nestjs/mongoose';
import { Inject, Injectable } from '@nestjs/common';
import { ENVALID } from 'nestjs-envalid';
import { Config } from '../env/app.env';

@Injectable()
export class MongooseConfigService implements MongooseOptionsFactory {
  constructor(@Inject(ENVALID) private readonly config: Config) {}
  createMongooseOptions():
    | Promise<MongooseModuleOptions>
    | MongooseModuleOptions {
    return {
      uri: this.config.MONGO_URI,
    };
  }
}
