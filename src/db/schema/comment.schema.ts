import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({
  timestamps: true,
})
export class Comment {
  @Prop() author: string;
  @Prop() content: string;
  // the object id of the comment this is under
  // all comments exist under at least one parent
  @Prop() parentType: ParentType;
  @Prop() parent: string;
}

export type CommentDocument = Comment & Document;

export enum ParentType {
  post = 'POST',
  comment = 'COMMENT',
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
