import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Comment } from './comment.schema';
import { Document } from 'mongoose';

@Schema({
  timestamps: true,
})
export class Post {
  @Prop() title: string;
  @Prop() author: string;
  @Prop() content: string; // could use a markdown parser here if desired
  @Prop() tags?: string[];
}

export type PostDocument = Post & Document;

export class PostWithComments extends Post {
  comments: Comment[];
}

export const PostSchema = SchemaFactory.createForClass(Post);
