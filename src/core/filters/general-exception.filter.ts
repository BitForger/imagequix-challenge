import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { PinoLogger } from 'nestjs-pino';

@Catch()
export class GeneralExceptionFilter<T> implements ExceptionFilter {
  constructor(private logger: PinoLogger) {}
  catch(exception: T, host: ArgumentsHost) {
    if (!(exception instanceof HttpException)) {
      this.logger.error({
        msg: 'Caught exception',
        ...exception,
      });
    }
  }
}
