import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { LeanDocument, Model } from 'mongoose';
import { Post, PostDocument } from '../../../db/schema/post.schema';
import { CreatePostDto } from '../../controller/dto/create-post.dto';
import { CommentService } from '../../../comment/services/comment/comment.service';
import { UpdatePostDto } from '../../controller/dto/update-post.dto';
import { ContentService } from '../../../services/content/content.service';

@Injectable()
export class PostService extends ContentService {
  constructor(
    @InjectModel(Post.name) private readonly postModel: Model<PostDocument>,
    private readonly commentService: CommentService,
  ) {
    super();
  }

  async create(postContent: CreatePostDto) {
    const post = new this.postModel(postContent);
    try {
      await post.save();
    } catch (e) {
      throw new InternalServerErrorException({
        msg: 'Unable to save new post',
        e,
      });
    }
  }

  async getAll() {
    const posts: LeanDocument<PostDocument>[] = await this.postModel
      .find({})
      .lean()
      .exec();

    if (posts.length < 1) {
      throw new NotFoundException({
        msg: 'No posts found',
      });
    }

    return posts;
  }

  async get(
    postId: string,
    options: { includeComments: boolean } = { includeComments: false },
  ) {
    let returnObject;
    const post = await this.postModel.findOne({ _id: postId }).lean().exec();

    if (!post) {
      throw new NotFoundException({
        msg: 'Could not find post',
        id: postId,
      });
    }

    if (options.includeComments) {
      const comments = await this.commentService.getCommentsForParent(
        postId,
        false,
      );
      returnObject = {
        comments,
        ...post,
      };
    }

    // always fall back to sending just the post
    return returnObject ?? post;
  }

  async delete(postId: string) {
    const result = await this.postModel.deleteOne({ _id: postId }).exec();
    return this.processDeleteResult(result, postId);
  }

  async update(postId: string, updatePostDto: UpdatePostDto) {
    return await this.postModel
      .findOneAndUpdate(
        {
          _id: postId,
        },
        updatePostDto,
      )
      .lean()
      .exec();
  }
}
