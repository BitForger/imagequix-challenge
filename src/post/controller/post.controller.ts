import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { PostService } from '../services/post/post.service';
import { UpdatePostDto } from './dto/update-post.dto';

@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}
  @Put()
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(new ValidationPipe({ whitelist: true, forbidUnknownValues: true }))
  async create(@Body() newPostDto: CreatePostDto) {
    await this.postService.create(newPostDto);
  }

  @Get()
  async getAll() {
    return this.postService.getAll();
  }

  @Get(':id')
  async get(
    @Param('id') postId: string,
    @Query('includeComments') includeComments = false,
  ) {
    return this.postService.get(postId, { includeComments });
  }

  @Post(':id')
  async update(
    @Param('id') postId: string,
    @Body() updatePostDto: UpdatePostDto,
  ) {
    await this.postService.update(postId, updatePostDto);
  }

  @Delete(':id')
  async delete(@Param('id') postId: string) {
    return this.postService.delete(postId);
  }
}
