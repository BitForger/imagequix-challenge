import { Module } from '@nestjs/common';
import { PostService } from './services/post/post.service';
import { PostController } from './controller/post.controller';
import { CommentModule } from '../comment/comment.module';

@Module({
  imports: [CommentModule],
  controllers: [PostController],
  providers: [PostService],
})
export class PostModule {}
