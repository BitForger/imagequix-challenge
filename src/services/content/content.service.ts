import { BadRequestException, Injectable } from '@nestjs/common';

@Injectable()
export abstract class ContentService {
  protected processDeleteResult(result, contentId) {
    if (result.deletedCount === 0) {
      throw new BadRequestException({
        msg: "Post doesn't exist",
        id: contentId,
      });
    }

    return {
      deletedCount: result.deletedCount,
      deleted: result.deletedCount > 0,
    };
  }
}
