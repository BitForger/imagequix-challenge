import { IsOptional, IsString } from 'class-validator';

export class UpdateCommentDto {
  @IsString()
  @IsOptional()
  author: string;

  @IsString()
  @IsOptional()
  content: string;
}
