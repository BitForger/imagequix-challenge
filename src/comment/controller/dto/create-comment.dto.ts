import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { ParentType } from '../../../db/schema/comment.schema';

export class CreateCommentDto {
  @IsString()
  @IsNotEmpty()
  author: string;

  @IsString()
  @IsNotEmpty()
  content: string;

  @IsString()
  @IsNotEmpty()
  parent: string;

  @IsEnum(ParentType)
  @IsNotEmpty()
  parentType: ParentType;
}
