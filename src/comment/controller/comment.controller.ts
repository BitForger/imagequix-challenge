import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ParentType } from '../../db/schema/comment.schema';
import { CommentService } from '../services/comment/comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Put()
  @HttpCode(HttpStatus.CREATED)
  @UsePipes(new ValidationPipe({ whitelist: true, forbidUnknownValues: true }))
  async create(@Body() createCommentDto: CreateCommentDto) {
    return this.commentService.create(createCommentDto);
  }

  @Get()
  async getComment(@Query('id') commentId: string) {
    return this.commentService.getComment(commentId);
  }

  @Get(':parent')
  async getCommentsForParent(
    @Param('parent') parentId: string,
    @Query('includeParent') includeParent = false,
    @Query('parentType') parentType: ParentType,
  ) {
    return this.commentService.getCommentsForParent(
      parentId,
      includeParent,
      parentType,
    );
  }

  @Post(':id')
  async updateComment(
    @Param('id') id: string,
    @Body() updateCommentDto: UpdateCommentDto,
  ) {
    return this.commentService.updateComment(id, updateCommentDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.commentService.delete(id);
  }
}
