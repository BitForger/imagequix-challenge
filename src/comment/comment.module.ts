import { Module } from '@nestjs/common';
import { CommentService } from './services/comment/comment.service';
import { CommentController } from './controller/comment.controller';

@Module({
  imports: [],
  providers: [CommentService],
  controllers: [CommentController],
  exports: [CommentService],
})
export class CommentModule {}
