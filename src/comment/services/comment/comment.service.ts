import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  Comment,
  CommentDocument,
  ParentType,
} from '../../../db/schema/comment.schema';
import { LeanDocument, Model } from 'mongoose';
import { CreateCommentDto } from '../../controller/dto/create-comment.dto';
import { UpdateCommentDto } from '../../controller/dto/update-comment.dto';
import { ContentService } from '../../../services/content/content.service';
import { Post, PostDocument } from '../../../db/schema/post.schema';

@Injectable()
export class CommentService extends ContentService {
  constructor(
    @InjectModel(Comment.name)
    private readonly commentModel: Model<CommentDocument>,
    @InjectModel(Post.name)
    private readonly postModel: Model<PostDocument>,
  ) {
    super();
  }

  async getCommentsForParent(
    postId: string,
    includeParent: boolean,
    parentType?: ParentType,
  ) {
    let returnObject:
      | (LeanDocument<CommentDocument> & {
          comments?: LeanDocument<CommentDocument>[];
        })
      | LeanDocument<CommentDocument>[];
    const comments = await this.commentModel
      .find({ parent: postId, parentType })
      .lean()
      .exec();

    if (includeParent) {
      returnObject = await (parentType === ParentType.comment
        ? this.commentModel
        : this.postModel
      )
        .findOne({ _id: postId })
        .lean()
        .exec();
      returnObject.comments = comments;
    } else {
      returnObject = comments;
    }

    return returnObject ?? [];
  }

  async getComment(
    id: string,
    { children }: { children?: boolean } = { children: false },
  ) {
    const comment: LeanDocument<
      CommentDocument & { children?: LeanDocument<CommentDocument>[] }
    > = await this.commentModel.findOne({ _id: id }).lean().exec();

    if (children) {
      comment.children = await this.commentModel
        .find({
          parent: comment._id.toString(),
          parentType: ParentType.comment,
        })
        .lean()
        .exec();
    }

    return comment;
  }

  async create(createCommentDto: CreateCommentDto) {
    const comment = new this.commentModel(createCommentDto);
    try {
      await comment.save();
    } catch (e) {
      throw new InternalServerErrorException({
        msg: 'Unable to save new comment',
        e,
      });
    }
  }

  async updateComment(id: string, updateCommentDto: UpdateCommentDto) {
    return await this.commentModel
      .findOneAndUpdate({ _id: id }, updateCommentDto)
      .lean()
      .exec();
  }

  async delete(id: string) {
    const result = await this.commentModel.deleteOne({ _id: id }).exec();
    return this.processDeleteResult(result, id);
  }
}
