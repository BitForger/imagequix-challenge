import { makeValidators, num, Static, str } from 'nestjs-envalid';

export const validators = makeValidators({
  NODE_ENV: str({ choices: ['development', 'production'] }),
  PORT: num({ default: 3000 }),
  MONGO_URI: str(),
});

export type Config = Static<typeof validators>;
