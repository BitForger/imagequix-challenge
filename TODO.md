The skills test consists of creating a very simple API that allows the consumer to create blog posts and comments on those blog posts. There's no need to build a front end that consumes the API.
You can use Postman/Insomnia/anything you prefer to validate the endpoints work.

Create a NodeJS REST API that allows:

- CRUD operations for a post (minimum fields: title, author, content, timestamp)

- CRUD operations for comments (minimum fields: author, content) on a post, including threaded comments*

- Persisting the data somewhere (this can even be in memory)

*Extras (NOT required)*

- Authentication
- Payload validation
- Tests
- Typescript usage

*Threaded comments means that a comment can be a reply to the post itself, or to another comment on the post, thus leading to a thread of comments.

Please push the code to a repository somewhere where we can clone & run it.