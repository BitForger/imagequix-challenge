# Imagequix Challenge

## Description

[Nest](https://github.com/nestjs/nest) project

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ docker compose up
```
